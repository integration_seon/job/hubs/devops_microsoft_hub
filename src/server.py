from rabbitmqX.patterns.server.work_queue_task_server import Work_Queue_Task_Server
from service import Services
queue_name = "integration.microsoftdevops"

services = Services()

rpc_server = Work_Queue_Task_Server(queue_name,services)
rpc_server.start()
