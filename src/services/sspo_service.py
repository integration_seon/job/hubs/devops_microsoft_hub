from sspo_db.application import factories
from requestx.RequestX import RequestX
import logging
logging.basicConfig(level=logging.INFO)
class SSPOService():

    def __init__(self):

        self.application_pp = factories.ApplicationFactory()
        self.organization_pp = factories.OrganizationFactory()
        self.configuration_pp = factories.ConfigurationFactory()
        self.content = None

    def __config(self, journal):
        data = journal["data"]
        self.organization_uuid = data['organization_uuid']
        real_time = ""
        if "content" in data:
            self.content = data['content']
            real_time = "_real_time"

        context = data['domain']

        self.url = "http://localhost:8080/api/experimental/dags/ms_devops_"+context+"_data_base_integration"+real_time+"/dag_runs"
    
        self.organization = self.organization_pp.get_by_uuid(self.organization_uuid)

    def __execute(self,):
        try:
            application = self.application_pp.retrive_by_name("tfs")

            configurations = self.configuration_pp.retrive_by_organization_and_application(self.organization, application)
            
            for configuration in configurations:
                
                organization = str(self.organization_uuid)
                tfs_key = configuration.secret
                tfs_url = configuration.url
                configuration_uuid = str(configuration.uuid)
                    
                request_x = RequestX()
                data = None
                if self.content is "":
                    data = {'organization_uuid': organization, 
                        "secret": tfs_key, 
                        "url": tfs_url, 
                        "configuration_uuid": configuration_uuid} 
                else:
                    data = {'organization_uuid': organization, 
                        "secret": tfs_key, 
                        "url": tfs_url, 
                        "configuration_uuid": configuration_uuid, 
                        "content": self.content} 
                
                data = {"conf": data}
                logging.info("SSPO_Service:"+ self.url)
                logging.info("SSPO_Service:"+ str(data))
                
                request_x.post(data, self.url)   

        except Exception as e: 
            logging.error("OS error: {0}".format(e))
            logging.error(e.__dict__)  

    def do(self,journal):
        try:
            self.__config(journal)
            self.__execute()

            logging.info("SSPO_Service: End")
        
        except Exception as e: 
            logging.error("OS error: {0}".format(e))
            logging.error(e.__dict__)  
         
            