import logging
logging.basicConfig(level=logging.INFO)
from .sspo_service import SSPOService

class Real_Time_SSPO_Service(SSPOService):

    def __init__(self):
        logging.error("Real Time SSPO Service: Start")
        url = "http://localhost:8080/api/experimental/dags/devops_microsoft_seon_sync_etl/dag_runs"
        super().__init__()
